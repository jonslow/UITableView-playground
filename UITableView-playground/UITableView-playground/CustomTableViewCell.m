//
//  CustomTableViewCell.m
//  UITableView-playground
//
//  Created by Yan Qian on 12/16/15.
//  Copyright © 2015 Yan Qian. All rights reserved.
//

#import "CustomTableViewCell.h"

@implementation CustomTableViewCell

- (void)awakeFromNib {
    // Initialization code
    NSLog(@"%@", @"cell awakeFromNib");
}

- (void)setSelected:(BOOL)selected animated:(BOOL)animated {
    [super setSelected:selected animated:animated];

    // Configure the view for the selected state
}

- (void)prepareForReuse
{
    [super prepareForReuse];
    NSLog(@"%@", @"cell prepareForReuse");
    
    self.titleLabel.text = @"";
    self.accessoryView = nil;
    self.accessoryType = UITableViewCellAccessoryNone;
}

@synthesize entity = _entity;

- (void)setEntity:(id)entity {
    _entity = entity;
    
//    [NSThread sleepForTimeInterval:0.1f];
}

@end
