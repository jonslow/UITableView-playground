//
//  AppDelegate.h
//  UITableView-playground
//
//  Created by Yan Qian on 12/16/15.
//  Copyright © 2015 Yan Qian. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface AppDelegate : UIResponder <UIApplicationDelegate>

@property (strong, nonatomic) UIWindow *window;


@end

