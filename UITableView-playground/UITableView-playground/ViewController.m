//
//  ViewController.m
//  UITableView-playground
//
//  Created by Yan Qian on 12/16/15.
//  Copyright © 2015 Yan Qian. All rights reserved.
//

#import "ViewController.h"
#import "CustomTableViewCell.h"

@interface ViewController ()

@end

@implementation ViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    // Do any additional setup after loading the view, typically from a nib.
    
    [self initViews];
}

- (void)initViews
{
    // 去除 tableView 顶部多余空白
    self.automaticallyAdjustsScrollViewInsets = NO; // iOS 7.0 +
    self.tableView.tableHeaderView = [[UIView alloc] initWithFrame:CGRectMake(0, 0, CGFLOAT_MIN, CGFLOAT_MIN)]; // 高度需要设成 CGFLOAT_MIN，设为0有些情况下会被忽略
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

#pragma mark - UITableViewDataSource

- (NSInteger)numberOfSectionsInTableView:(UITableView *)tableView
{
    NSLog(@"%@", @"table numOfSections");
    return 5;
}

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section
{
    NSLog(@"%@: %ld", @"table numOfRowsInSection", (long)section);
    return 3;
}

- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath
{
    NSLog(@"%@: %ld - %ld", @"table cellForRowAtIndexPath", (long)indexPath.section, (long)indexPath.row);
    static NSString * const identifier = @"defaultCell";
    CustomTableViewCell *cell =  [tableView dequeueReusableCellWithIdentifier:identifier];

    if (!cell)
    {
        [tableView registerNib:[UINib nibWithNibName:@"CustomTableViewCell" bundle:nil] forCellReuseIdentifier:identifier];
        cell = [tableView dequeueReusableCellWithIdentifier:identifier];
    }
    
    if (indexPath.section < 2)
    {
        cell.titleLabel.text = [NSString stringWithFormat:@"%ld - %ld", (long)indexPath.section, (long)indexPath.row];
        
        if (indexPath.row == 1)
        {
            cell.accessoryType = UITableViewCellAccessoryDisclosureIndicator;
        }
        else if (indexPath.row == 2)
        {
            cell.accessoryView = [[UIImageView alloc] initWithImage:[UIImage imageNamed:@"arrow_grey"]];
        }
    }
    
    if (indexPath.section == 2 && indexPath.row == 0)
    {
        cell.titleLabel.text = [NSString stringWithFormat:@"%ld - %ld", (long)indexPath.section, (long)indexPath.row];
    }
        

    cell.entity = [[NSMutableDictionary alloc] init];
    
    
    return cell;
}

- (nullable NSString *)tableView:(UITableView *)tableView titleForHeaderInSection:(NSInteger)section
{
//    NSLog(@"%@: %ld", @"table titleForHeaderInSection", (long)section);
    return [NSString stringWithFormat:@"section %ld", (long)section];
}

#pragma mark - UITableViewDelegate
- (void)tableView:(UITableView *)tableView willDisplayCell:(UITableViewCell *)cell forRowAtIndexPath:(NSIndexPath *)indexPath
{
    NSLog(@"%@: %ld - %ld", @"table willDisplayCellAtIndexPath", (long)indexPath.section, (long)indexPath.row);
    // section 0：分隔线左右充满
    if (indexPath.section == 0)
    {
        // 如果 superView 设置了 layout margin，不继承 tableView 的 margin
        if ([cell respondsToSelector:@selector(setPreservesSuperviewLayoutMargins:)]) {
            // iOS 8.0+
            [cell setPreservesSuperviewLayoutMargins:NO];
        }
        
        if ([cell respondsToSelector:@selector(setSeparatorInset:)]) {
            // iOS 7，这个方法在 iOS 8 下无效
            [cell setSeparatorInset:UIEdgeInsetsZero];
        }
        
        if ([cell respondsToSelector:@selector(setLayoutMargins:)]) {
            // iOS 8.0 +
            [cell setLayoutMargins:UIEdgeInsetsZero];
        }
    }

    // section 1：隐藏分隔线
    else if (indexPath.section == 1 && indexPath.row == 0)
    {
        // iOS 7.x 中可以使用 UIEdgeInsetsMake(0, 0, 0, -CGRectGetWidth(self.bounds))
        // 但上面的方法在 iOS 8.x 中不再有效，EdgeInsetsMake 中传入的负值会被自动改写为0
        cell.separatorInset = UIEdgeInsetsMake(0, CGRectGetWidth(self.tableView.bounds)/2.0, 0, CGRectGetWidth(self.tableView.bounds)/2.0);
    }

    // section 2 and beyong：
    else
    {
        // iOS 7
        if ([cell respondsToSelector:@selector(setSeparatorInset:)]) {
            [cell setSeparatorInset:UIEdgeInsetsMake(8, 20, 8, 20)];
        }
        
        // iOS 8.0 +
        if ([cell respondsToSelector:@selector(setLayoutMargins:)]) {
            [cell setLayoutMargins:UIEdgeInsetsMake(8, 20, 8, 20)];
            [cell.contentView setLayoutMargins:UIEdgeInsetsMake(8, 15, 8, 20)];
        }
        
        
    }
}


@end
