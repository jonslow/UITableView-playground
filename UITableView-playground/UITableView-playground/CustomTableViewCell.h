//
//  CustomTableViewCell.h
//  UITableView-playground
//
//  Created by Yan Qian on 12/16/15.
//  Copyright © 2015 Yan Qian. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface CustomTableViewCell : UITableViewCell

@property (weak, nonatomic) IBOutlet UILabel *titleLabel;

@property (strong, nonatomic) id entity;


@end
